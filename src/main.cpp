/*
    SPDX-License-Identifier: GPL-2.0-or-later
    SPDX-FileCopyrightText: 2022 J K <>
*/

#include <QApplication>
#include <QQmlApplicationEngine>
#include <QUrl>
#include <QtQml>

#include "about.h"
#include "app.h"
#include "version-kirigamidemo.h"
#include <KAboutData>
#include <KLocalizedContext>
#include <KLocalizedString>

#include "kirigamidemoconfig.h"

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);
    QCoreApplication::setOrganizationName(QStringLiteral("KDE"));
    QCoreApplication::setApplicationName(QStringLiteral("kirigamidemo"));

    KAboutData aboutData(
                         // The program name used internally.
                         QStringLiteral("kirigamidemo"),
                         // A displayable program name string.
                         i18nc("@title", "kirigamidemo"),
                         // The program version string.
                         QStringLiteral(KIRIGAMIDEMO_VERSION_STRING),
                         // Short description of what the app does.
                         i18n("Application Description"),
                         // The license this code is released under.
                         KAboutLicense::GPL,
                         // Copyright Statement.
                         i18n("(c) 2022"));
    aboutData.addAuthor(i18nc("@info:credit", "AUTHOR"), i18nc("@info:credit", "Author Role"), QStringLiteral(""), QStringLiteral("https://yourwebsite.com"));
    KAboutData::setApplicationData(aboutData);

    QQmlApplicationEngine engine;

    auto config = kirigamidemoConfig::self();

    qmlRegisterSingletonInstance("org.kde.kirigamidemo", 1, 0, "Config", config);

    AboutType about;
    qmlRegisterSingletonInstance("org.kde.kirigamidemo", 1, 0, "AboutType", &about);

    App application;
    qmlRegisterSingletonInstance("org.kde.kirigamidemo", 1, 0, "App", &application);

    engine.rootContext()->setContextObject(new KLocalizedContext(&engine));
    engine.load(QUrl(QStringLiteral("qrc:///main.qml")));

    if (engine.rootObjects().isEmpty()) {
        return -1;
    }

    return app.exec();
}
